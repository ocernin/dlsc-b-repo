#!/usr/bin/env python
# coding: utf-8


env_path = ""
on_collab = False

if on_collab: 
    import sys
    from google.colab import drive
    drive.mount('/content/drive')
    dlsc_path = 'MyDrive/ETH/DLSC/B/' # TODO set this

    env_path = f'/content/drive/{dlsc_path}'

    if env_path not in sys.path:
        sys.path.append(env_path)    


import os
import errno

eq_name = 'ac'
model_name = "AC"

#####DO NOT CHANGE
causal = True

if(causal == False):
    eq_name = eq_name + "-standard"
    model_name =  model_name + "-Standard"
else:
    model_name = model_name + "-Causal"

    
def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
            
make_sure_path_exists(env_path+"models/"+eq_name)
make_sure_path_exists(env_path+"output/"+eq_name+"/refplot")
make_sure_path_exists(env_path+"output/"+eq_name+"/wplot")


import torch.nn as nn
import torch
import os

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
torch.manual_seed(42)

class NeuralNet(nn.Module):

    def __init__(self, input_dimension, output_dimension, n_hidden_layers, neurons, regularization_param, regularization_exp, retrain_seed):
        super(NeuralNet, self).__init__()
        # Number of input dimensions n
        self.input_dimension = input_dimension
        # Number of output dimensions m
        self.output_dimension = output_dimension
        # Number of neurons per layer
        self.neurons = neurons
        # Number of hidden layers
        self.n_hidden_layers = n_hidden_layers
        # Activation function
        self.activation = nn.Tanh()
        self.regularization_param = regularization_param
        # Regularization exponent
        self.regularization_exp = regularization_exp
        # Random seed for weight initialization

        self.input_layer = nn.Linear(self.input_dimension, self.neurons)
        self.hidden_layers = nn.ModuleList([nn.Linear(self.neurons, self.neurons) for _ in range(n_hidden_layers - 1)])
        self.output_layer = nn.Linear(self.neurons, self.output_dimension)
        self.retrain_seed = retrain_seed
        # Random Seed for weight initialization
        self.init_xavier()

    def forward(self, x):
        # The forward function performs the set of affine and non-linear transformations defining the network
        # (see equation above)
        x = self.activation(self.input_layer(x))
        for k, l in enumerate(self.hidden_layers):
            x = self.activation(l(x))
        return self.output_layer(x)

    def init_xavier(self):
        torch.manual_seed(self.retrain_seed)

        def init_weights(m):
            if type(m) == nn.Linear and m.weight.requires_grad and m.bias.requires_grad:
                g = nn.init.calculate_gain('tanh')
                torch.nn.init.xavier_uniform_(m.weight, gain=g)
                # torch.nn.init.xavier_normal_(m.weight, gain=g)
                m.bias.data.fill_(0)

        self.apply(init_weights)

    def regularization(self):
        reg_loss = 0
        for name, param in self.named_parameters():
            if 'weight' in name:
                reg_loss = reg_loss + torch.norm(param, self.regularization_exp)
        return self.regularization_param * reg_loss


def fit(model, training_set, num_epochs, optimizer, p, verbose=True):
    history = list()

    # Loop over epochs
    for epoch in range(num_epochs):
        if verbose: print("################################ ", epoch, " ################################")

        running_loss = list([0])

        # Loop over batches
        for j, (x_train_, u_train_) in enumerate(training_set):
            def closure():
                # zero the parameter gradients
                optimizer.zero_grad()
                # forward + backward + optimize
                u_pred_ = model(x_train_)
                # Item 1. below
                loss = torch.mean((u_pred_.reshape(-1, ) - u_train_.reshape(-1, )) ** p) + model.regularization()
                # Item 2. below
                loss.backward()
                # Compute average training loss over batches for the current epoch
                running_loss[0] += loss.item()
                return loss

            # Item 3. below
            optimizer.step(closure=closure)

        if verbose: print('Loss: ', (running_loss[0] / len(training_set)))
        history.append(running_loss[0])

    return history


class Legendre(nn.Module):
    """ Univariate Legendre Polynomial """

    def __init__(self, PolyDegree):
        super(Legendre, self).__init__()
        self.degree = PolyDegree

    def legendre(self,x, degree):
        x = x.reshape(-1, 1)
        list_poly = list()
        zeroth_pol = torch.ones(x.size(0),1)
        list_poly.append(zeroth_pol)
        # retvar[:, 0] = x * 0 + 1
        if degree > 0:
            first_pol = x
            list_poly.append(first_pol)
            ith_pol = torch.clone(first_pol)
            ith_m_pol = torch.clone(zeroth_pol)

            for ii in range(1, degree):
                ith_p_pol = ((2 * ii + 1) * x * ith_pol - ii * ith_m_pol) / (ii + 1)
                list_poly.append(ith_p_pol)
                ith_m_pol = torch.clone(ith_pol)
                ith_pol = torch.clone(ith_p_pol)
        list_poly = torch.cat(list_poly,1)
        return list_poly

    def forward(self, x):
        eval_poly = self.legendre(x, self.degree)
        return eval_poly

class MultiVariatePoly(nn.Module):

    def __init__(self, dim, order):
        super(MultiVariatePoly, self).__init__()
        self.order = order
        self.dim = dim
        self.polys = Legendre(order)
        self.num = (order + 1) ** dim
        self.linear = torch.nn.Linear(self.num, 1)

    def forward(self, x):
        poly_eval = list()
        leg_eval = torch.cat([self.polys(x[:, i]).reshape(1, x.shape[0], self.order + 1) for i in range(self.dim) ])
        for i in range(x.shape[0]):
            poly_eval.append(torch.torch.cartesian_prod(*leg_eval[:, i, :]).prod(dim=1).view(1, -1))
        poly_eval = torch.cat(poly_eval)
        return self.linear(poly_eval)


import torch.optim as optim
import torch.utils
import torch.utils.data
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt
import numpy as np
import scipy
import pandas as pd


class Pinns:
    def __init__(self, n_int_, n_sb_, n_tb_, causal_):
        
        self.causal = causal_
        
        self.n_int = n_int_
        self.n_sb = n_sb_
        self.n_tb = n_tb_
        
        self.iter_total=0
        
        self.print = False
        self.loop_counter=0
        self.print_freq=500
        self.frame_counter=0
        self.frame_number=0
        self.frame_freq=50
        self.animation=True
        
        self.lic = []
        self.lsb = []
        self.lr = []
        self.l_ref = []
        

        # Extrema of the solution domain (t,x) in [0,0.1]x[-1,1]
        self.domain_extrema = torch.tensor([[0, 1],  # Time dimension
                                            [-1, 1]])  # Space dimension

        # Number of space dimensions
        self.space_dimensions = 1

        # Parameter to balance role of data and PDE
        self.lambda_u = 1000
        
        #import reference solution
        self.ref_data = scipy.io.loadmat('ref/AC.mat')

        # F Dense NN to approximate the solution of the underlying heat equation
        self.approximate_solution = NeuralNet(input_dimension=self.domain_extrema.shape[0], output_dimension=1,
                                              n_hidden_layers=4,
                                              neurons=128,
                                              regularization_param=0.,
                                              regularization_exp=2.,
                                              retrain_seed=42)
        '''self.approximate_solution = MultiVariatePoly(self.domain_extrema.shape[0], 3)'''

        # Generator of Sobol sequences
        self.soboleng = torch.quasirandom.SobolEngine(dimension=self.domain_extrema.shape[0])

        # Training sets S_sb, S_tb, S_int as torch dataloader
        self.training_set_sb, self.training_set_tb, self.training_set_int = self.assemble_datasets()

    ################################################################################################
    # Function to linearly transform a tensor whose value are between 0 and 1
    # to a tensor whose values are between the domain extrema
    def convert(self, tens):
        assert (tens.shape[1] == self.domain_extrema.shape[0])
        return tens * (self.domain_extrema[:, 1] - self.domain_extrema[:, 0]) + self.domain_extrema[:, 0]

    # Initial condition
    def initial_condition(self, x):
        return x*x*torch.cos(np.pi * x)

    def get_reference_data(self):
        t = self.ref_data["tt"]
        x = self.ref_data["x"]
        u_ref = self.ref_data['uu']
        t_ref, x_ref = np.meshgrid(t, x)
        nt=t_ref.shape[1]
        nx=t_ref.shape[0]
        grid = np.zeros((nx*nt,2))
        for i in range(0,nx):
            grid[i*nt:(i+1)*nt,1]=x_ref[i]
            grid[i*nt:(i+1)*nt,0]=t_ref[i]

        input_int_ref = torch.from_numpy(grid).type(torch.float32)
        
        return input_int_ref,u_ref, t_ref, x_ref
    
    ######################    
    def save_losses(self):
        df = pd.DataFrame([self.lic, self.lsb, self.lr]) 
        df = df.transpose() 
        df.columns = ['IC Loss', 'SB Loss', 'R Loss']
        df.to_csv(env_path+'output/'+eq_name+'/losses.csv', sep=',')

        df_ref = pd.DataFrame([self.l_ref]) 
        df_ref = df_ref.transpose() 
        df_ref.columns = ['Error', ]
        df_ref.to_csv(env_path+'output/'+eq_name+'/errors.csv', sep=',') 


    ################################################################################################
    # Function returning the input-output tensor required to assemble the training set S_tb corresponding to the temporal boundary
    def add_temporal_boundary_points(self):
        t0 = self.domain_extrema[0, 0]
        input_tb = self.convert(self.soboleng.draw(self.n_tb))
        input_tb[:, 0] = torch.full(input_tb[:, 0].shape, t0)
        output_tb = self.initial_condition(input_tb[:, 1]).reshape(-1, 1)

        return input_tb, output_tb

    # Function returning the input-output tensor required to assemble the training set S_sb corresponding to the spatial boundary
    def add_spatial_boundary_points(self):
        x0 = self.domain_extrema[1, 0]
        xL = self.domain_extrema[1, 1]

        input_sb = self.convert(self.soboleng.draw(self.n_sb))

        input_sb_0 = torch.clone(input_sb)
        input_sb_0[:, 1] = torch.full(input_sb_0[:, 1].shape, x0)

        input_sb_L = torch.clone(input_sb)
        input_sb_L[:, 1] = torch.full(input_sb_L[:, 1].shape, xL)

        output_sb_0 = torch.zeros((input_sb.shape[0], 1))
        output_sb_L = torch.zeros((input_sb.shape[0], 1))

        return torch.cat([input_sb_0, input_sb_L], 0), torch.cat([output_sb_0, output_sb_L], 0)

    #  Function returning the input-output tensor required to assemble the training set S_int corresponding to the interior domain where the PDE is enforced
    def add_interior_points(self): #Changed so that we have n_tb amount of points at different points in time, given by the times tensor
        if(self.causal):
            t0 = self.domain_extrema[0, 0]
            tL = self.domain_extrema[0, 1]
            times = torch.linspace(t0, tL, self.n_int)
            input_int = torch.zeros(self.n_int - 1, self.n_tb, 2)
            for i in range(times.shape[0]):
                if (i == 0):
                    continue

                input = self.convert(self.soboleng.draw(self.n_tb))
                input[:, 0] = torch.full(input[:, 0].shape, times[i])
                input_int[i - 1] = input
            output_int = torch.zeros(self.n_int - 1, input_int.shape[1], 1)
        else:
            input_int = self.convert(self.soboleng.draw(self.n_int))
            output_int = torch.zeros((input_int.shape[0], 1))
            return input_int, output_int

        return input_int, output_int

    # Function returning the training sets S_sb, S_tb, S_int as dataloader
    def assemble_datasets(self):
        input_sb, output_sb = self.add_spatial_boundary_points()   # S_sb
        input_tb, output_tb = self.add_temporal_boundary_points()  # S_tb
        input_int, output_int = self.add_interior_points()         # S_int

        training_set_sb = DataLoader(torch.utils.data.TensorDataset(input_sb, output_sb), batch_size=2*self.space_dimensions*self.n_sb, shuffle=False)
        training_set_tb = DataLoader(torch.utils.data.TensorDataset(input_tb, output_tb), batch_size=self.n_tb, shuffle=False)
        training_set_int = DataLoader(torch.utils.data.TensorDataset(input_int, output_int), batch_size=self.n_int, shuffle=False)

        return training_set_sb, training_set_tb, training_set_int

    ################################################################################################
    # Function to compute the terms required in the definition of the TEMPORAL boundary residual
    def apply_initial_condition(self, input_tb):
        u_pred_tb = self.approximate_solution(input_tb)
        return u_pred_tb

    # Function to compute the terms required in the definition of the SPATIAL boundary residual
    def apply_boundary_conditions(self, input_sb):
        input_sb_0, input_sb_L = input_sb.split(int(input_sb.shape[0]/2))
        input_sb_0.requires_grad = True
        input_sb_L.requires_grad = True
        u_pred_sb_0 = self.approximate_solution(input_sb_0).reshape(-1, )
        u_pred_sb_L = self.approximate_solution(input_sb_L).reshape(-1, )
        res = u_pred_sb_0 - u_pred_sb_L

        grad_x_0 = torch.autograd.grad(u_pred_sb_0.sum(), input_sb_0, create_graph=True)[0][:, 1]
        grad_x_L = torch.autograd.grad(u_pred_sb_L.sum(), input_sb_L, create_graph=True)[0][:, 1]
        res_grad = grad_x_0 - grad_x_L

        return res, res_grad

    # Function to compute the PDE residuals
    def compute_pde_residual(self, input_int):
        input_int.requires_grad = True
        u = self.approximate_solution(input_int).reshape(-1, )
        u3 = u*u*u

        grad_u = torch.autograd.grad(u.sum(), input_int, create_graph=True)[0]
        grad_u_t = grad_u[:, 0]
        grad_u_x = grad_u[:, 1]
        grad_u_xx = torch.autograd.grad(grad_u_x.sum(), input_int, create_graph=True)[0][:, 1]

        residual = grad_u_t - 0.0001 * grad_u_xx + 5 * u3 - 5 * u
        return residual.reshape(-1, )

    # Function to compute the total loss (weighted sum of spatial boundary loss, temporal boundary loss and interior loss)
    def compute_loss(self, inp_train_sb, u_train_sb, inp_train_tb, u_train_tb, inp_train_int, eps, verbose=True):
        u_pred_sb, u_pred_sb_grad = self.apply_boundary_conditions(inp_train_sb)
        u_pred_tb = self.apply_initial_condition(inp_train_tb)

        r_sb = u_pred_sb
        r_sb_grad = u_pred_sb_grad
        r_tb = u_train_tb - u_pred_tb

        loss_sb = torch.mean(abs(r_sb) ** 2)
        loss_sb_grad = torch.mean(abs(r_sb_grad) ** 2)
        loss_tb = torch.mean(abs(r_tb) ** 2)

        assert (u_pred_tb.shape[1] == u_train_tb.shape[1])
        weights = torch.ones(self.n_int)
        int_losses = torch.zeros(self.n_int)
        for i in range(self.n_int):
            if (i == 0):
                int_losses[0] = self.lambda_u * loss_tb
                continue
            r_int = self.compute_pde_residual(inp_train_int[i - 1])
            int_losses[i] = torch.mean(abs(r_int) ** 2)
        loss_sum=0
        if(causal):
            for i in range(self.n_int):
                if (i == 0 or i == 1):
                    continue
                loss_sum += int_losses[i]
                weights[i] = torch.exp(-eps * loss_sum)
                
            loss_int = (1 / self.n_int) * torch.sum(weights * int_losses)             

            loss = torch.log10(loss_sb + loss_int)
        
        else:
            r_int = self.compute_pde_residual(inp_train_int)
            loss_int = torch.mean(abs(r_int) ** 2)             
            loss = torch.log10(self.lambda_u * loss_tb + loss_sb + loss_sb_grad + loss_int)
        
        if verbose: print(str(self.iter_total)+"|Total loss: ", round(loss.item(), 4), "| IC Loss: ", round(torch.log10(loss_tb).item(), 4), "| BC Loss: ", round(torch.log10(loss_sb).item(), 4),"| Function Loss: ", round(torch.log10(loss_int).item(), 4))
        if self.print and (self.loop_counter <=0):
            self.plotting_sb(inp_train_sb, u_train_sb)
            self.plotting_tb(inp_train_tb, u_train_tb)
            self.plotting()
            self.plotting_w(weights)
            self.loop_counter=self.print_freq
        self.loop_counter=self.loop_counter-1
        
        if self.animation:
            self.lic.append(torch.log10(loss_tb).item())
            self.lr.append(torch.log10(loss_int).item())
            self.lsb.append(torch.log10(loss_sb).item())
            if (self.frame_counter >= self.frame_freq):
                self.save_animation_frame()
                self.save_weight_frame(int_losses, weights)
                self.frame_counter = 0
                self.frame_number += 1
            self.frame_counter += 1
            
        self.iter_total += 1
        
        return loss, weights

    ################################################################################################
    def fit(self, num_epochs, optimizer, verbose=True):
        history = list()
        epsilons = torch.tensor([0.01, 0.1, 1, 10, 100])
        min_flag = False
        delta = 0.99
        # Loop over epochs
        for eps in epsilons:
            for epoch in range(num_epochs):
                if verbose: print("################################ ", epoch, " ################################")

                for j, ((inp_train_sb, u_train_sb), (inp_train_tb, u_train_tb), (inp_train_int, u_train_int)) in enumerate(zip(self.training_set_sb, self.training_set_tb, self.training_set_int)):
                    def closure():
                        optimizer.zero_grad()
                        loss, self.weights = self.compute_loss(inp_train_sb, u_train_sb, inp_train_tb, u_train_tb, inp_train_int, eps, verbose=verbose)
                        loss.backward()
                        history.append(loss.item())
                        return loss

                    optimizer.step(closure=closure)
                    if ((torch.min(self.weights[2:]) > 0.99) and self.causal ):
                        min_flag = True
                
                if (min_flag):
                    min_flag = False
                    break
                torch.save(pinn, env_path+"models/"+eq_name+"/"+model_name+"Training.pkl")
                self.save_losses()
                
        print('Final Loss: ', history[-1])
        return history

    ################################################################################################
    def plotting(self, save = False):       
        input_ref, u_ref, t_ref, x_ref = self.get_reference_data()

        plt.rcParams.update({'font.size': 14})
        output = self.approximate_solution(input_ref).reshape((t_ref.shape)).detach()

        fig, axs = plt.subplots(1, 3, figsize=(18, 4), dpi=150)
        im1 = axs[0].pcolor(t_ref, x_ref, output, cmap='jet',vmin=-1, vmax=1)
        axs[0].set_xlabel("t")
        axs[0].set_ylabel("x")
        plt.colorbar(im1, ax=axs[0])
        axs[0].grid(True, which="both", ls=":")

        im2 = axs[1].pcolor(t_ref, x_ref, u_ref, cmap='jet',vmin=-1, vmax=1)
        axs[1].set_xlabel("t")
        axs[1].set_ylabel("x")
        plt.colorbar(im2, ax=axs[1])
        axs[1].grid(True, which="both", ls=":")

        u_ref_tens = torch.from_numpy(u_ref).type(torch.float32)
        err = abs(output - u_ref_tens)
        im3 = axs[2].pcolor(t_ref, x_ref, err.detach(), cmap='jet')
        axs[2].set_xlabel("t")
        axs[2].set_ylabel("x")
        plt.colorbar(im3, ax=axs[2])
        axs[2].grid(True, which="both", ls=":")

        axs[0].set_title("Approximate Solution")
        axs[1].set_title("Reference Solution")
        axs[2].set_title("Error")

        err_norm = (torch.mean((err) ** 2) / torch.mean(u_ref_tens ** 2)) ** 0.5 * 100
        print("L2 Relative Error Norm: ", err_norm.item(), "%")
        
        if(save == True):
            self.l_ref.append(err_norm.item())
            fig.savefig(fname=env_path+"output/"+eq_name+"/refplot/img_"+str(self.frame_number).zfill(5)+".png")
        else:
            plt.show()
            
        plt.close('all')
                
    def plotting_sb(self,input_sb_, train_sb_):
        #input_sb_.requires_grad = True
        f = self.approximate_solution(input_sb_)
        #grad_f = torch.autograd.grad(f.sum(), input_sb_, create_graph=True)[0]
        #grad_f_x_tens = grad_f[:, 1]
        #grad_f_x = (grad_f_x_tens.detach().numpy()).flatten()

        input_t = input_sb_[:,0]
        input_x = input_sb_[:,1]
        f_np = (f.detach().numpy()).flatten()

        fig, axs = plt.subplots(1, 2, figsize=(16, 8), dpi=150)
        im1 = axs[0].scatter(input_sb_[:self.n_sb, 0].detach(), f[:self.n_sb].detach() )
        axs[0].set_xlabel("t")
        axs[0].set_ylabel("")
        axs[0].grid(True, which="both", ls=":")
        im2 = axs[1].scatter(input_sb_[self.n_sb:, 0].detach(), f[self.n_sb:].detach() )
        axs[1].set_xlabel("t")
        axs[1].set_ylabel("")
        axs[1].grid(True, which="both", ls=":")
        axs[0].set_title("f - x=-1")
        axs[1].set_title("f - x=1")

        plt.show()

        #fig, axs = plt.subplots(1, 2, figsize=(16, 8), dpi=150)
        #im1 = axs[0].scatter(input_sb_[:self.n_sb, 0].detach(), grad_f_x[:self.n_sb] )
        #axs[0].set_xlabel("t")
        #axs[0].set_ylabel("")
        #axs[0].grid(True, which="both", ls=":")
        #im2 = axs[1].scatter(input_sb_[self.n_sb:, 0].detach(), grad_f_x[self.n_sb:] )
        #axs[1].set_xlabel("t")
        #axs[1].set_ylabel("")
        #axs[1].grid(True, which="both", ls=":")
        #axs[0].set_title("grad f_x - x=-1")
        #axs[1].set_title("grad f_x - x=1")

        #plt.show()
        plt.close('all')

    def plotting_tb(self,input_tb_, train_tb_):
        f = self.approximate_solution(input_tb_)

        fig, axs = plt.subplots(1, 2, figsize=(16, 8), dpi=150)
        im1 = axs[0].scatter(input_tb_[:, 1].detach(), f[:].detach() )
        axs[0].set_xlabel("x")
        axs[0].set_ylabel("")
        axs[0].grid(True, which="both", ls=":")
        im2 = axs[1].scatter(input_tb_[:, 1].detach(), train_tb_.detach() )
        axs[1].set_xlabel("x")
        axs[1].set_ylabel("")
        axs[1].grid(True, which="both", ls=":")
        axs[0].set_title("f pred - t=0")
        axs[1].set_title("f truth - t=0")

        plt.show()
        plt.close('all')
        
    def plotting_w(self,weights):
        plt.figure(figsize=(16, 8), dpi=150)
        plt.plot(np.linspace(0,1,n_int), weights.detach())
        plt.xlabel("t")
        plt.ylabel("w")
        plt.ylim(0,1)
        plt.grid(True, which="both", ls=":")

        plt.show()
        plt.close('all')        
        
    def save_animation_frame(self):
        self.plotting(save=True)
        return
    
    def save_weight_frame(self,losses,weights):
        fig, axs = plt.subplots(1, 3, figsize=(18, 4), dpi=150)
        im1 = axs[0].plot(self.lic,label="IC Loss")
        im1 = axs[0].plot(self.lr,label="PDE Loss")
        im1 = axs[0].plot(self.lsb,label="SB Loss")
        axs[0].set_xlabel("Iteration #")
        axs[0].set_ylabel("Loss")
        axs[0].grid(True, which="both", ls=":")
        axs[0].legend(loc='upper right')

        im2 = axs[1].plot(np.linspace(0.,1.,weights.shape[0]-1),losses.detach()[1:])
        axs[1].set_xlabel("t")
        axs[1].set_ylabel("Loss")
        axs[1].grid(True, which="both", ls=":")

        im3 = axs[2].plot(np.linspace(0.,1.,weights.shape[0]),weights.detach())
        axs[2].set_xlabel("t")
        axs[2].set_ylabel("w")
        axs[2].grid(True, which="both", ls=":")
        axs[2].set_ylim(0,1)

        axs[0].set_title("Training Loss")
        axs[1].set_title("PDE Loss")
        axs[2].set_title("Weights")

        fig.savefig(fname=env_path+"output/"+eq_name+"/wplot/img_"+str(self.frame_number).zfill(5)+".png")
        plt.close('all')


n_int = 128
n_sb = 128
n_tb = 100

pinn = Pinns(n_int, n_sb, n_tb, causal)


n_epochs = 50
optimizer_LBFGS = optim.LBFGS(pinn.approximate_solution.parameters(),
                              lr=float(0.5),
                              max_iter=250,
                              max_eval=50000,
                              history_size=150,
                              line_search_fn="strong_wolfe",
                              tolerance_change=1.0 * np.finfo(float).eps)
optimizer_ADAM = optim.Adam(pinn.approximate_solution.parameters(),
                            lr=float(0.001))


hist = pinn.fit(num_epochs=n_epochs,
                optimizer=optimizer_LBFGS,
                verbose=True)

import pandas as pd

df = pd.DataFrame([pinn.lic, pinn.lsb, pinn.lr]) 
df = df.transpose() 
df.columns = ['IC Loss', 'SB Loss', 'R Loss']
df.to_csv(env_path+'output/'+eq_name+'/losses.csv', sep=',')

df_ref = pd.DataFrame([pinn.l_ref]) 
df_ref = df_ref.transpose() 
df_ref.columns = ['Error', ]
df_ref.to_csv(env_path+'output/'+eq_name+'/errors.csv', sep=',')


torch.save(pinn, env_path+"models/"+eq_name+"/"+model_name+".pkl")

#pinn = torch.load(env_path+"models/"+eq_name+"/"+model_name+".pkl")
